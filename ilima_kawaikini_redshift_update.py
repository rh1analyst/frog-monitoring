import numpy as np
import pandas as pd
import os
import math
import calendar
from filechunkio import FileChunkIO
from itertools import accumulate
import boto.s3
import psycopg2
import gzip
from sqlalchemy import create_engine, Table, Column, String, MetaData, Float, Numeric, DateTime, Float, Integer, Numeric, select
from datetime import datetime, timezone
now = datetime.now(timezone.utc)
print(now.strftime("%Y-%m-%d %H:%M:%S") + ' (UTC): pulling ' + now.strftime('%Y') + " " + now.strftime('%B') + " data from L+U server")
df_kww = pd.read_csv("https://coolretrievebeta.herokuapp.com/csv_data_m/3/" + str(now.year) + "/" + str(now.month) + "/")
df_kwe = pd.read_csv("https://coolretrievebeta.herokuapp.com/csv_data_m/4/" + str(now.year) + "/" + str(now.month) + "/")
df_ilima = pd.read_csv("https://coolretrievebeta.herokuapp.com/csv_data_m/11/" + str(now.year) + "/" + str(now.month) + "/")
df_kw = pd.read_csv("https://coolretrievebeta.herokuapp.com/csv_data_m/5/" + str(now.year) + "/" + str(now.month) + "/")
df_ewa = pd.read_csv("https://coolretrievebeta.herokuapp.com/csv_data_m/15/" + str(now.year) + "/" + str(now.month) + "/")
df_kww.drop(['Room_Air_Speed', 'Room_Air_Temperature_C'], axis=1, inplace=True)
df_kww.rename(columns = {"AIR_1_E":"air_temperature_e","AIR_2_W":"air_temperature_w","FSF_1_SE":"floor_surface_temperature_se","FSF_2_Center":"floor_surface_temperature_center","FSF_3_NW":"floor_surface_temperature_nw","PLE_1_SE":"plenum_temperature_se","PLE_2_Center":"plenum_temperature_center","PLE_3_NW":"plenum_temperature_nw","WAL_1_E":"wall_surface_temperature_e","WAL_2_W":"wall_surface_temperature_w","PV_Inverter_central_neg_kW":"PV_Inverter_1_neg_kW","PV_Inverter_central_neg_kWh":"PV_Inverter_1_neg_kWh","PV_Inverter_central_pos_kW":"PV_Inverter_1_pos_kW","PV_Inverter_central_pos_kWh":"PV_Inverter_1_pos_kWh","PV_Inverter_micro_neg_kW":"PV_Inverter_2_neg_kW","PV_Inverter_micro_neg_kWh":"PV_Inverter_2_neg_kWh","PV_Inverter_micro_pos_kW":"PV_Inverter_2_pos_kW","PV_Inverter_micro_pos_kWh":"PV_Inverter_2_pos_kWh","Suppy_Air_Humidity":"Supply_Air_Humidity"}, inplace=True)
df_kwe.rename(columns = {"AIR_3_E":"air_temperature_e","AIR_4_W":"air_temperature_w","FSF_4_SE":"floor_surface_temperature_se","FSF_5_Center":"floor_surface_temperature_center","FSF_6_NW":"floor_surface_temperature_nw","PLE_4_SE":"plenum_temperature_se","PLE_5_Center":"plenum_temperature_center","PLE_6_NW":"plenum_temperature_nw","WAL_3_E":"wall_surface_temperature_e","WAL_4_W":"wall_surface_temperature_w","Suppy_Air_Humidity":"Supply_Air_Humidity"}, inplace=True)
df_ilima.rename(columns = {"E wall air temperature":"air_temperature_e","W wall air temperature":"air_temperature_w","SE floor surface temperature":"floor_surface_temperature_se","Center floor surface temperature":"floor_surface_temperature_center","NW floor surface temperature":"floor_surface_temperature_nw","SE plenum air temperature":"plenum_temperature_se","Center plenum air temperature":"plenum_temperature_center","NW plenum air temperature":"plenum_temperature_nw","E wall surface temperature":"wall_surface_temperature_e","W wall surface temperature":"wall_surface_temperature_w","Exhaust_Fans_kW":"Exhaust_Fan_kW","Exhaust_Fans_kWh":"Exhaust_Fan_kWh","Suppy_Air_Humidity":"Supply_Air_Humidity","Lighting_Exterior_kW":"Lighting_Wall_and_Exterior_kW","Lighting_Exterior_kWh":"Lighting_Wall_and_Exterior_kWh"}, inplace=True)
for d in [df_kww, df_kwe, df_ilima]:    
    d.drop(['Panel_Feed_kW','Panel_Feed_kWh'], axis = 1, inplace = True)     
for d in [df_kww, df_kwe, df_ilima, df_kw, df_ewa]:
    d["UTC time"] = d["UTC time"].apply(lambda x: datetime.strptime(x, "%Y-%m-%d %H:%M:%S"))
    d["UTC time"] = d["UTC time"].map(lambda x: x.replace(second = 0))
    d.set_index("UTC time", inplace = True)    
    d.replace(['missing','None'], np.nan, inplace = True)
    for col in d.columns.tolist():
        d[col] = d[col].astype(float)
print(datetime.now(timezone.utc).strftime("%Y-%m-%d %H:%M:%S") + ' (UTC): uploading ' + now.strftime('%Y') + " " + now.strftime('%B') + " reported data to S3")
if not df_kww.empty:
    df_kww["Source"] = 3
if not df_kwe.empty:
    df_kwe["Source"] = 4
if not df_ilima.empty:
    df_ilima["Source"] = 11
if not df_kw.empty:
    df_kw["Source"] = 5
if not df_ewa.empty:
    df_ewa["Source"] = 15
df_building_reported = pd.concat([df_kww,df_kwe,df_ilima]).reset_index()
if not df_building_reported.empty:    
    df_building_reported = df_building_reported[['UTC time','Ceiling_Fans_kW','Ceiling_Fans_kWh','floor_surface_temperature_center','plenum_temperature_center','Condensing_Unit_kW','Condensing_Unit_kWh','air_temperature_e','wall_surface_temperature_e','Exhaust_Fan_kW','Exhaust_Fan_kWh','Fan_Coil_Unit_kW','Fan_Coil_Unit_kWh','Lighting_Main_Space_kW','Lighting_Main_Space_kWh','floor_surface_temperature_nw','plenum_temperature_nw','PV_Inverter_1_neg_kW','PV_Inverter_1_neg_kWh','PV_Inverter_1_pos_kW','PV_Inverter_1_pos_kWh','PV_Inverter_2_neg_kW','PV_Inverter_2_neg_kWh','PV_Inverter_2_pos_kW','PV_Inverter_2_pos_kWh','Panel_Feed_neg_kW','Panel_Feed_neg_kWh','Panel_Feed_pos_kW','Panel_Feed_pos_kWh','Room_Air_CO2','Room_Air_Humidity','Room_Air_Speed','Room_Illuminance_Ceiling','Room_Illuminance_WestWall','floor_surface_temperature_se','plenum_temperature_se','Supply_Air_Temperature_C','Supply_Air_Humidity','air_temperature_w','wall_surface_temperature_w','Source','Lighting_Wall_and_Exterior_kW','Lighting_Wall_and_Exterior_kWh','Louver_Actuator_kW','Louver_Actuator_kWh','Room_Air_Temperature_C']]
    df_building_reported['Source'] = df_building_reported['Source'].astype(int)
df_weather_reported = pd.concat([df_kw,df_ewa]).reset_index()        
if not df_weather_reported.empty:
    df_weather_reported = df_weather_reported[['UTC time','Batt','DewPt','Gust Speed','Pressure','RH','Rain','Solar Rad','Solar Rad1','Source','Temp','Wind Dir','Wind Speed']]
    df_weather_reported['Source'] = df_weather_reported['Source'].astype(int)
AWS_ACCESS_KEY_ID = 'AKIAILTPHWSDQAUKCLVQ'
AWS_SECRET_ACCESS_KEY = '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'
s3_conn = boto.connect_s3(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY)
bucket = s3_conn.get_bucket('rh1-extract')
chunk_size = 10485760
if not df_building_reported.empty:
    upload_file_path_building = 'ilima_kawaikini_building_data_reported_monthly/ilima_kawaikini_building_data_reported_' + str(now.year) + '_' + str(now.month) + '.csv.gz'
    df_building_reported.to_csv(upload_file_path_building, compression='gzip', header=False, index=False)
    mp = bucket.initiate_multipart_upload(upload_file_path_building)
    source_size = os.stat(upload_file_path_building).st_size
    chunk_count = int(math.ceil(source_size / float(chunk_size)))
    for i in range(chunk_count):
        offset = chunk_size * i
        bytes = min(chunk_size, source_size - offset)
        with FileChunkIO(upload_file_path_building, 'r', offset=offset, bytes=bytes) as fp:
            mp.upload_part_from_file(fp, part_num=i + 1)
    mp.complete_upload()
    os.remove(upload_file_path_building)
if not df_weather_reported.empty:
    upload_file_path_weather = 'ilima_kawaikini_weather_data_reported_monthly/ilima_kawaikini_weather_data_reported_' + str(now.year) + '_' + str(now.month) + '.csv.gz'
    df_weather_reported.to_csv(upload_file_path_weather, compression='gzip', header=False, index=False)
    mp = bucket.initiate_multipart_upload(upload_file_path_weather)
    source_size = os.stat(upload_file_path_weather).st_size
    chunk_count = int(math.ceil(source_size / float(chunk_size)))
    for i in range(chunk_count):
        offset = chunk_size * i
        bytes = min(chunk_size, source_size - offset)
        with FileChunkIO(upload_file_path_weather, 'r', offset=offset, bytes=bytes) as fp:
            mp.upload_part_from_file(fp, part_num=i + 1)
    mp.complete_upload()
    os.remove(upload_file_path_weather)
if not df_kww.empty:
    df_kww.drop(['Source'], axis=1, inplace=True)
if not df_kwe.empty:
    df_kwe.drop(['Source'], axis=1, inplace=True)
if not df_ilima.empty:
    df_ilima.drop(['Source'], axis=1, inplace=True)
if not df_kw.empty:
    df_kw.drop(['Source'], axis=1, inplace=True)
if not df_ewa.empty:
    df_ewa.drop(['Source'], axis=1, inplace=True)
print(datetime.now(timezone.utc).strftime("%Y-%m-%d %H:%M:%S") + ' (UTC): calculating panel feed values')
def panelFeedFunction_kw(test):    
    net_consumption_kW_list = [0]
    net_consumption_kWh_list = [0]
    PV_production_kW_list = [0]
    PV_production_kWh_list = [0]
    unmonitored_end_uses_kW_list = [0]
    unmonitored_end_uses_kWh_list = [0]
    total_consumption_kW_list = [0]
    total_consumption_kWh_list = [0]
    test["Condensing_Unit_kW"] = test["Condensing_Unit_kW"]*2
    test["Condensing_Unit_kWh"] = test["Condensing_Unit_kWh"]*2    
    test["Fan_Coil_Unit_kW"] = test["Fan_Coil_Unit_kW"]*2    
    test["Fan_Coil_Unit_kWh"] = test["Fan_Coil_Unit_kWh"]*2        
    for n in range(1,len(test)):    
        net_consumption_kW = ((test.ix[n]["Panel_Feed_pos_kWh"] - test.ix[n - 1]["Panel_Feed_pos_kWh"]) - (test.ix[n]["Panel_Feed_neg_kWh"] - test.ix[n - 1]["Panel_Feed_neg_kWh"])) * 6
        PV_production_kW = (((test.ix[n]["PV_Inverter_1_neg_kWh"] - test.ix[n - 1]["PV_Inverter_1_neg_kWh"]) - (test.ix[n]["PV_Inverter_1_pos_kWh"] - test.ix[n - 1]["PV_Inverter_1_pos_kWh"])) + ((test.ix[n]["PV_Inverter_2_neg_kWh"] - test.ix[n - 1]["PV_Inverter_2_neg_kWh"]) - (test.ix[n]["PV_Inverter_2_pos_kWh"] - test.ix[n - 1]["PV_Inverter_2_pos_kWh"]))) * 6 * 4
        total_consumption_kW = net_consumption_kW + PV_production_kW
        unmonitored_end_uses_kW = total_consumption_kW - ((test.ix[n]["Ceiling_Fans_kWh"] - test.ix[n - 1]["Ceiling_Fans_kWh"]) + (test.ix[n]["Condensing_Unit_kWh"] - test.ix[n - 1]["Condensing_Unit_kWh"]) + (test.ix[n]["Exhaust_Fan_kWh"] - test.ix[n - 1]["Exhaust_Fan_kWh"]) + (test.ix[n]["Fan_Coil_Unit_kWh"] - test.ix[n - 1]["Fan_Coil_Unit_kWh"]) + (test.ix[n]["Lighting_Main_Space_kWh"] - test.ix[n - 1]["Lighting_Main_Space_kWh"]) + (test.ix[n]["Lighting_Wall_and_Exterior_kWh"] - test.ix[n - 1]["Lighting_Wall_and_Exterior_kWh"]) + (test.ix[n]["Louver_Actuator_kWh"] - test.ix[n - 1]["Louver_Actuator_kWh"])) * 6
        net_consumption_kW_list.append(net_consumption_kW)
        net_consumption_kWh_list.append(net_consumption_kW / 6)
        PV_production_kW_list.append(PV_production_kW)
        PV_production_kWh_list.append(PV_production_kW / 6)
        total_consumption_kW_list.append(total_consumption_kW) 
        total_consumption_kWh_list.append(total_consumption_kW / 6)
        unmonitored_end_uses_kW_list.append(unmonitored_end_uses_kW)
        unmonitored_end_uses_kWh_list.append(unmonitored_end_uses_kW / 6)
    test["net_consumption_kW"] = net_consumption_kW_list
    test["net_consumption_kWh"] = net_consumption_kWh_list
    test["PV_production_kW"] = PV_production_kW_list
    test["PV_production_kWh"] = PV_production_kWh_list
    test["total_consumption_kW"] = total_consumption_kW_list
    test["total_consumption_kWh"] = total_consumption_kWh_list
    test["unmonitored_end_uses_kW"] = unmonitored_end_uses_kW_list
    test["unmonitored_end_uses_kWh"] = unmonitored_end_uses_kWh_list
    test["Ceiling_Fans_kWh"] = test["Ceiling_Fans_kW"]/6
    test["Condensing_Unit_kWh"] = test["Condensing_Unit_kW"]/6
    test["Exhaust_Fan_kWh"] = test["Exhaust_Fan_kW"]/6
    test["Fan_Coil_Unit_kWh"] = test["Fan_Coil_Unit_kW"]/6
    test["Lighting_Wall_and_Exterior_kWh"] = test["Lighting_Wall_and_Exterior_kW"]/6
    test["Lighting_Main_Space_kWh"] = test["Lighting_Main_Space_kW"]/6
    test["Louver_Actuator_kWh"] = test["Louver_Actuator_kW"]/6
def panelFeedFunction_ilima(test):    
    net_consumption_kW_list = [0]
    net_consumption_kWh_list = [0]
    PV_production_kW_list = [0]
    PV_production_kWh_list = [0]
    unmonitored_end_uses_kW_list = [0]
    unmonitored_end_uses_kWh_list = [0]
    total_consumption_kW_list = [0]
    total_consumption_kWh_list = [0]
    test["Condensing_Unit_kW"] = test["Condensing_Unit_kW"]*2
    test["Condensing_Unit_kWh"] = test["Condensing_Unit_kWh"]*2    
    test["Fan_Coil_Unit_kW"] = test["Fan_Coil_Unit_kW"]*2    
    test["Fan_Coil_Unit_kWh"] = test["Fan_Coil_Unit_kWh"]*2        
    for n in range(1,len(test)):
        net_consumption_kW = ((test.ix[n]["Panel_Feed_pos_kWh"] - test.ix[n - 1]["Panel_Feed_pos_kWh"]) - (test.ix[n]["Panel_Feed_neg_kWh"] - test.ix[n - 1]["Panel_Feed_neg_kWh"])) * 6
        PV_production_kW = (((test.ix[n]["PV_Inverter_1_neg_kWh"] - test.ix[n - 1]["PV_Inverter_1_neg_kWh"]) - (test.ix[n]["PV_Inverter_1_pos_kWh"] - test.ix[n - 1]["PV_Inverter_1_pos_kWh"])) + ((test.ix[n]["PV_Inverter_2_neg_kWh"] - test.ix[n - 1]["PV_Inverter_2_neg_kWh"]) - (test.ix[n]["PV_Inverter_2_pos_kWh"] - test.ix[n - 1]["PV_Inverter_2_pos_kWh"]))) * 6 * 4
        total_consumption_kW = net_consumption_kW + PV_production_kW
        unmonitored_end_uses_kW = total_consumption_kW - ((test.ix[n]["Ceiling_Fans_kWh"] - test.ix[n - 1]["Ceiling_Fans_kWh"]) + (test.ix[n]["Condensing_Unit_kWh"] - test.ix[n - 1]["Condensing_Unit_kWh"]) + (test.ix[n]["Exhaust_Fan_kWh"] - test.ix[n - 1]["Exhaust_Fan_kWh"]) + (test.ix[n]["Fan_Coil_Unit_kWh"] - test.ix[n - 1]["Fan_Coil_Unit_kWh"]) + (test.ix[n]["Lighting_Main_Space_kWh"] - test.ix[n - 1]["Lighting_Main_Space_kWh"])) * 6
        net_consumption_kW_list.append(net_consumption_kW)
        net_consumption_kWh_list.append(net_consumption_kW / 6)
        PV_production_kW_list.append(PV_production_kW)
        PV_production_kWh_list.append(PV_production_kW / 6)
        total_consumption_kW_list.append(total_consumption_kW) 
        total_consumption_kWh_list.append(total_consumption_kW / 6)
        unmonitored_end_uses_kW_list.append(unmonitored_end_uses_kW)
        unmonitored_end_uses_kWh_list.append(unmonitored_end_uses_kW / 6)
    test["net_consumption_kW"] = net_consumption_kW_list
    test["net_consumption_kWh"] = net_consumption_kWh_list
    test["PV_production_kW"] = PV_production_kW_list
    test["PV_production_kWh"] = PV_production_kWh_list
    test["total_consumption_kW"] = total_consumption_kW_list
    test["total_consumption_kWh"] = total_consumption_kWh_list
    test["unmonitored_end_uses_kW"] = unmonitored_end_uses_kW_list
    test["unmonitored_end_uses_kWh"] = unmonitored_end_uses_kWh_list
    test["Ceiling_Fans_kWh"] = test["Ceiling_Fans_kW"]/6
    test["Condensing_Unit_kWh"] = test["Condensing_Unit_kW"]/6
    test["Exhaust_Fan_kWh"] = test["Exhaust_Fan_kW"]/6
    test["Fan_Coil_Unit_kWh"] = test["Fan_Coil_Unit_kW"]/6
    test["Lighting_Main_Space_kWh"] = test["Lighting_Main_Space_kW"]/6
if not df_ilima.empty:
    panelFeedFunction_ilima(df_ilima)
if not df_kww.empty:
    if not df_kwe.empty:
        panelFeedFunction_kw(df_kww)
        panelFeedFunction_kw(df_kwe)                                                               
    else:
        panelFeedFunction_kw(df_kww)
else:
    if not df_kwe.empty:
        panelFeedFunction_kw(df_kwe)
df_kww.drop(['PV_Inverter_1_neg_kW', 'PV_Inverter_1_neg_kWh', 'PV_Inverter_1_pos_kW', 'PV_Inverter_1_pos_kWh', 'PV_Inverter_2_neg_kW', 'PV_Inverter_2_neg_kWh', 'PV_Inverter_2_pos_kW', 'PV_Inverter_2_pos_kWh', 'Panel_Feed_neg_kW', 'Panel_Feed_neg_kWh', 'Panel_Feed_pos_kW', 'Panel_Feed_pos_kWh'], axis=1, inplace=True)
df_kwe.drop(['PV_Inverter_1_neg_kW', 'PV_Inverter_1_neg_kWh', 'PV_Inverter_1_pos_kW', 'PV_Inverter_1_pos_kWh', 'PV_Inverter_2_neg_kW', 'PV_Inverter_2_neg_kWh', 'PV_Inverter_2_pos_kW', 'PV_Inverter_2_pos_kWh', 'Panel_Feed_neg_kW', 'Panel_Feed_neg_kWh', 'Panel_Feed_pos_kW', 'Panel_Feed_pos_kWh'], axis=1, inplace=True)
df_ilima.drop(['PV_Inverter_1_neg_kW', 'PV_Inverter_1_neg_kWh', 'PV_Inverter_1_pos_kW', 'PV_Inverter_1_pos_kWh', 'PV_Inverter_2_neg_kW', 'PV_Inverter_2_neg_kWh', 'PV_Inverter_2_pos_kW', 'PV_Inverter_2_pos_kWh', 'Panel_Feed_neg_kW', 'Panel_Feed_neg_kWh', 'Panel_Feed_pos_kW', 'Panel_Feed_pos_kWh'], axis=1, inplace=True)
print(datetime.now(timezone.utc).strftime("%Y-%m-%d %H:%M:%S") + ' (UTC): using forward fill to replace outlier data')
rangeDict = {0:[0,5],1:[0,20],2:[0,50],3:[0,100],4:[0,350],5:[250,2000],6:[-20,20]}
n = 0
kww_good = 0
kww_low = 0
kww_low_timestamps = []
kww_low_cols = []
kww_high = 0
kww_high_timestamps = []
kww_high_cols = []
kww_missing = 0
kww_missing_timestamps = []
kww_missing_cols = []
kwe_good = 0
kwe_low = 0
kwe_low_timestamps = []
kwe_low_cols = []
kwe_high = 0
kwe_high_timestamps = []
kwe_high_cols = []
kwe_missing = 0
kwe_missing_timestamps = []
kwe_missing_cols = []
ilima_good = 0
ilima_low = 0
ilima_low_timestamps = []
ilima_low_cols = []
ilima_high = 0
ilima_high_timestamps = []
ilima_high_cols = []
ilima_missing = 0
ilima_missing_timestamps = []
ilima_missing_cols = []
for group in [["Ceiling_Fans_kW","Exhaust_Fan_kW","Fan_Coil_Unit_kW","Lighting_Main_Space_kW","Lighting_Wall_and_Exterior_kW","Louver_Actuator_kW"], ["Condensing_Unit_kW","Room_Air_Speed","unmonitored_end_uses_kW","total_consumption_kW","Ceiling_Fans_kWh","Condensing_Unit_kWh","Exhaust_Fan_kWh","Fan_Coil_Unit_kWh","Lighting_Main_Space_kWh","Lighting_Wall_and_Exterior_kWh","Louver_Actuator_kWh","unmonitored_end_uses_kWh","total_consumption_kWh"], ["air_temperature_e","air_temperature_w","floor_surface_temperature_se","floor_surface_temperature_center","floor_surface_temperature_nw","plenum_temperature_se","plenum_temperature_center","plenum_temperature_nw","Room_Air_Temperature_C","Supply_Air_Temperature_C","wall_surface_temperature_e","wall_surface_temperature_w"], ["Room_Air_Humidity","Supply_Air_Humidity"], ["Room_Illuminance_Ceiling","Room_Illuminance_WestWall"], ["Room_Air_CO2"], ["net_consumption_kWh","PV_production_kWh","net_consumption_kW","PV_production_kW"]]:
    for col in group:
        if col in df_kww.columns.tolist():
            for i,x in enumerate(df_kww[col]):
                if x >= rangeDict[n][0] and x <= rangeDict[n][1]:
                    kww_good += 1
                elif x < rangeDict[n][0]:
                    kww_low += 1
                    kww_low_timestamps.append(i)
                    kww_low_cols.append(col)
                elif x > rangeDict[n][1]:
                    kww_high += 1
                    kww_high_timestamps.append(i)
                    kww_high_cols.append(col)
                elif pd.isnull(x) == True:
                    kww_missing += 1
                    kww_missing_timestamps.append(i)
                    kww_missing_cols.append(col)
            df_kww[col] = df_kww[col].apply(lambda x: x if x >= rangeDict[n][0] and x <= rangeDict[n][1] else np.nan)
        if col in df_kwe.columns.tolist():
            for i,x in enumerate(df_kwe[col]):
                if x >= rangeDict[n][0] and x <= rangeDict[n][1]:
                    kwe_good += 1
                elif x < rangeDict[n][0]:
                    kwe_low += 1
                    kwe_low_timestamps.append(i)
                    kwe_low_cols.append(col)                    
                elif x > rangeDict[n][1]:
                    kwe_high += 1
                    kwe_high_timestamps.append(i)
                    kwe_high_cols.append(col)
                elif pd.isnull(x) == True:
                    kwe_missing += 1
                    kwe_missing_timestamps.append(i)
                    kwe_missing_cols.append(col)                    
            df_kwe[col] = df_kwe[col].apply(lambda x: x if x >= rangeDict[n][0] and x <= rangeDict[n][1] else np.nan)        
        if col in df_ilima.columns.tolist():
            for i,x in enumerate(df_ilima[col]):
                if x >= rangeDict[n][0] and x <= rangeDict[n][1]:
                    ilima_good += 1
                elif x < rangeDict[n][0]:
                    ilima_low += 1
                    ilima_low_timestamps.append(i)
                    ilima_low_cols.append(col)
                elif x > rangeDict[n][1]:
                    ilima_high += 1
                    ilima_high_timestamps.append(i)
                    ilima_high_cols.append(col)
                elif pd.isnull(x) == True:
                    ilima_missing += 1
                    ilima_missing_timestamps.append(i)
                    ilima_missing_cols.append(col)                    
            df_ilima[col] = df_ilima[col].apply(lambda x: x if x >= rangeDict[n][0] and x <= rangeDict[n][1] else np.nan)                            
    n+=1
df_kww_errors = pd.DataFrame(index=df_kww.index[list(set(kww_low_timestamps + kww_high_timestamps + kww_missing_timestamps))], columns=df_kww.columns).sort_index().fillna(0)
df_kwe_errors = pd.DataFrame(index=df_kwe.index[list(set(kwe_low_timestamps + kwe_high_timestamps + kwe_missing_timestamps))], columns=df_kwe.columns).sort_index().fillna(0)
df_ilima_errors = pd.DataFrame(index=df_ilima.index[list(set(ilima_low_timestamps + ilima_high_timestamps + ilima_missing_timestamps))], columns=df_ilima.columns).sort_index().fillna(0)
for t,c in zip(df_kww.index[kww_low_timestamps], kww_low_cols):
    df_kww_errors.set_value(t,c,4)
for t,c in zip(df_kww.index[kww_high_timestamps], kww_high_cols):
    df_kww_errors.set_value(t,c,3)
for t,c in zip(df_kww.index[kww_missing_timestamps], kww_missing_cols):
    df_kww_errors.set_value(t,c,2)
for t,c in zip(df_kwe.index[kwe_low_timestamps], kwe_low_cols):
    df_kwe_errors.set_value(t,c,4)
for t,c in zip(df_kwe.index[kwe_high_timestamps], kwe_high_cols):
    df_kwe_errors.set_value(t,c,3)
for t,c in zip(df_kwe.index[kwe_missing_timestamps], kwe_missing_cols):
    df_kwe_errors.set_value(t,c,2)
for t,c in zip(df_ilima.index[ilima_low_timestamps], ilima_low_cols):
    df_ilima_errors.set_value(t,c,4)
for t,c in zip(df_ilima.index[ilima_high_timestamps], ilima_high_cols):
    df_ilima_errors.set_value(t,c,3)
for t,c in zip(df_ilima.index[ilima_missing_timestamps], ilima_missing_cols):
    df_ilima_errors.set_value(t,c,2)
df_kww.fillna(method = "ffill", inplace = True)
df_kwe.fillna(method = "ffill", inplace = True)
df_ilima.fillna(method = "ffill", inplace = True)
rangeDict = {"Batt":[0,5], "DewPt":[0,50], "Gust Speed":[0,50], "Pressure":[850,1100], "RH":[0,100], "Rain":[0,190], "Solar Rad":[0,1500], "Solar Rad1":[0,1500], "Temp":[0,50], "Wind Dir":[0,360], "Wind Speed":[0,50]} 
kw_good = 0
kw_low = 0
kw_low_timestamps = []
kw_low_cols = []
kw_high = 0
kw_high_timestamps = []
kw_high_cols = []
kw_missing = 0
kw_missing_timestamps = []
kw_missing_cols = []
ewa_good = 0
ewa_low = 0
ewa_low_timestamps = []
ewa_low_cols = []
ewa_high = 0
ewa_high_timestamps = []
ewa_high_cols = []
ewa_missing = 0
ewa_missing_timestamps = []
ewa_missing_cols = []
for col in df_kw.columns.tolist():
    for i,x in enumerate(df_kw[col]):
        if x >= rangeDict[col][0] and x <= rangeDict[col][1]:
            kw_good += 1
        elif x < rangeDict[col][0]:
            kw_low += 1
            kw_low_timestamps.append(i)
            kw_low_cols.append(col)
        elif x > rangeDict[col][1]:
            kw_high += 1
            kw_high_timestamps.append(i)
            kw_high_cols.append(col)
        elif pd.isnull(x) == True:
            kw_missing += 1
            kw_missing_timestamps.append(i)
            kw_missing_cols.append(col)
df_kw[col] = df_kw[col].apply(lambda x: x if x >= rangeDict[col][0] and x <= rangeDict[col][1] else np.nan)    
for col in df_ewa.columns.tolist():
    for i,x in enumerate(df_ewa[col]):
        if x >= rangeDict[col][0] and x <= rangeDict[col][1]:
            ewa_good += 1
        elif x < rangeDict[col][0]:
            ewa_low += 1
            ewa_low_timestamps.append(i)
            ewa_low_cols.append(col)
        elif x > rangeDict[col][1]:
            ewa_high += 1
            ewa_high_timestamps.append(i)
            ewa_high_cols.append(col)
        elif pd.isnull(x) == True:
            ewa_missing += 1
            ewa_missing_timestamps.append(i)
            ewa_missing_cols.append(col)
df_ewa[col] = df_ewa[col].apply(lambda x: x if x >= rangeDict[col][0] and x <= rangeDict[col][1] else np.nan)  
df_kw_errors = pd.DataFrame(index=df_kw.index[list(set(kw_low_timestamps + kw_high_timestamps + kw_missing_timestamps))], columns=df_kw.columns).sort_index().fillna(0)
df_ewa_errors = pd.DataFrame(index=df_ewa.index[list(set(ewa_low_timestamps + ewa_high_timestamps + ewa_missing_timestamps))], columns=df_ewa.columns).sort_index().fillna(0)
for t,c in zip(df_kw.index[kw_low_timestamps], kw_low_cols):
    df_kw_errors.set_value(t,c,4)
for t,c in zip(df_kw.index[kw_high_timestamps], kw_high_cols):
    df_kw_errors.set_value(t,c,3)
for t,c in zip(df_kw.index[kw_missing_timestamps], kw_missing_cols):
    df_kw_errors.set_value(t,c,2)
for t,c in zip(df_ewa.index[ewa_low_timestamps], ewa_low_cols):
    df_ewa_errors.set_value(t,c,4)
for t,c in zip(df_ewa.index[ewa_high_timestamps], ewa_high_cols):
    df_ewa_errors.set_value(t,c,3)
for t,c in zip(df_ewa.index[ewa_missing_timestamps], ewa_missing_cols):
    df_ewa_errors.set_value(t,c,2)
df_kw.fillna(method = "ffill", inplace = True)
df_ewa.fillna(method = "ffill", inplace = True)
print(datetime.now(timezone.utc).strftime("%Y-%m-%d %H:%M:%S") + ' (UTC): inserting missing timestamps')
utc_times_10 = []
utc_times_5 = []
for d in range(1,now.day):
    for h in range(24):
        for m in [5,15,25,35,45,55]:
            utc_times_10.append(datetime(now.year,now.month,d,h,m,0,tzinfo=timezone.utc).strftime("%Y-%m-%d %H:%M:%S"))
        for m in [0,5,10,15,20,25,30,35,40,45,50,55]:
            utc_times_5.append(datetime(now.year,now.month,d,h,m,0,tzinfo=timezone.utc).strftime("%Y-%m-%d %H:%M:%S"))        
for h in range(now.hour):
    for m in [5,15,25,35,45,55]:
        utc_times_10.append(datetime(now.year,now.month,now.day,h,m,0,tzinfo=timezone.utc).strftime("%Y-%m-%d %H:%M:%S"))
    for m in [0,5,10,15,20,25,30,35,40,45,50,55]:
        utc_times_5.append(datetime(now.year,now.month,now.day,h,m,0,tzinfo=timezone.utc).strftime("%Y-%m-%d %H:%M:%S"))
for m in [i*10+5 for i in range((now.minute+5)//10)]:
    utc_times_10.append(datetime(now.year,now.month,now.day,now.hour,m,0,tzinfo=timezone.utc).strftime("%Y-%m-%d %H:%M:%S"))
for m in [i*5 for i in range((now.minute+5)//5)]:
    utc_times_5.append(datetime(now.year,now.month,now.day,now.hour,m,0,tzinfo=timezone.utc).strftime("%Y-%m-%d %H:%M:%S"))
df_10_min = pd.DataFrame(utc_times_10, columns=['UTC time']).set_index('UTC time')
df_5_min = pd.DataFrame(utc_times_5, columns=['UTC time']).set_index('UTC time')
if not df_kww.empty:
    df_kww_full = pd.concat([df_kww, df_10_min], axis=1)
    df_kww_errors_missing = pd.concat([df_kww_full[~df_kww_full.index.isin(df_kww.index)].fillna(1), df_kww_errors])
    df_kww_full.fillna(method = "ffill", inplace=True)
    for col in df_kww_errors_missing.columns.tolist():
        df_kww_errors_missing[col] = df_kww_errors_missing[col].astype(int)
    df_kww_full["Source"] = 3
    if not df_kww_errors_missing.empty:
        df_kww_errors_missing["Source"] = 3
else:
    df_kww_full = pd.DataFrame(data=[])
    df_kww_errors_missing = pd.DataFrame(data=[])
if not df_kwe.empty:
    df_kwe_full = pd.concat([df_kwe, df_10_min], axis=1)
    df_kwe_errors_missing = pd.concat([df_kwe_full[~df_kwe_full.index.isin(df_kwe.index)].fillna(1), df_kwe_errors])
    df_kwe_full.fillna(method = "ffill", inplace=True)
    for col in df_kwe_errors_missing.columns.tolist():
        df_kwe_errors_missing[col] = df_kwe_errors_missing[col].astype(int)
    df_kwe_full["Source"] = 4
    if not df_kwe_errors_missing.empty:
        df_kwe_errors_missing["Source"] = 4
else:
    df_kwe_full = pd.DataFrame(data=[])
    df_kwe_errors_missing = pd.DataFrame(data=[])
if not df_ilima.empty:
    df_ilima_full = pd.concat([df_ilima, df_10_min], axis=1)
    df_ilima_errors_missing = pd.concat([df_ilima_full[~df_ilima_full.index.isin(df_ilima.index)].fillna(1), df_ilima_errors])
    df_ilima_full.fillna(method = "ffill", inplace=True)
    for col in df_ilima_errors_missing.columns.tolist():
        df_ilima_errors_missing[col] = df_ilima_errors_missing[col].astype(int)
    df_ilima_full["Source"] = 11
    if not df_ilima_errors_missing.empty:
        df_ilima_errors_missing["Source"] = 11
else:
    df_ilima_full = pd.DataFrame(data=[])
    df_ilima_errors_missing = pd.DataFrame(data=[])
if not df_kw.empty:
    df_kw_full = pd.concat([df_kw, df_5_min], axis=1)
    df_kw_errors_missing = pd.concat([df_kw_full[~df_kw_full.index.isin(df_kw.index)].fillna(1), df_kw_errors])
    df_kw_full.fillna(method = "ffill", inplace=True)
    for col in df_kw_errors_missing.columns.tolist():
        df_kw_errors_missing[col] = df_kw_errors_missing[col].astype(int)
    df_kw_full["Source"] = 5
    if not df_kw_errors_missing.empty:
        df_kw_errors_missing["Source"] = 5
else:
    df_kw_full = pd.DataFrame(data=[])
    df_kw_errors_missing = pd.DataFrame(data=[])
if not df_ewa.empty:
    df_ewa_full = pd.concat([df_ewa, df_5_min], axis=1)
    df_ewa_errors_missing = pd.concat([df_ewa_full[~df_ewa_full.index.isin(df_ewa.index)].fillna(1), df_ewa_errors])
    df_ewa_full.fillna(method = "ffill", inplace=True)
    for col in df_ewa_errors_missing.columns.tolist():
        df_ewa_errors_missing[col] = df_ewa_errors_missing[col].astype(int)
    df_ewa_full["Source"] = 15
    if not df_ewa_errors_missing.empty:
        df_ewa_errors_missing["Source"] = 15
else:
    df_ewa_full = pd.DataFrame(data=[])
    df_ewa_errors_missing = pd.DataFrame(data=[])
print(datetime.now(timezone.utc).strftime("%Y-%m-%d %H:%M:%S") + ' (UTC): uploading ' + now.strftime('%Y') + " " + now.strftime('%B') + " processed data to S3")
if not df_kww_full.empty:
    if not df_kwe_full.empty:
        if not df_ilima_full.empty:
            df_building_processed = pd.concat([df_kww_full,df_kwe_full,df_ilima_full]).reset_index().rename(columns={'index':'UTC time'})
            df_building_errors_missing = pd.concat([df_kww_errors_missing,df_kwe_errors_missing,df_ilima_errors_missing]).reset_index().rename(columns={'index':'UTC time'})
        else:
            df_building_processed = pd.concat([df_kww_full,df_kwe_full]).reset_index().rename(columns={'index':'UTC time'})
            df_building_errors_missing = pd.concat([df_kww_errors_missing,df_kwe_errors_missing]).reset_index().rename(columns={'index':'UTC time'})
    else:
        if not df_ilima_full.empty:
            df_building_processed = pd.concat([df_kww_full,df_ilima_full]).reset_index().rename(columns={'index':'UTC time'})
            df_building_errors_missing = pd.concat([df_kww_errors_missing,df_ilima_errors_missing]).reset_index().rename(columns={'index':'UTC time'})
        else:
            df_building_processed = df_kww_full.reset_index().rename(columns={'index':'UTC time'})
            df_building_errors_missing = df_kww_errors_missing.reset_index().rename(columns={'index':'UTC time'})
else:
    if not df_kwe_full.empty:
        if not df_ilima_full.empty:
            df_building_processed = pd.concat([df_kwe_full,df_ilima_full]).reset_index().rename(columns={'index':'UTC time'})
            df_building_errors_missing = pd.concat([df_kwe_errors_missing,df_ilima_errors_missing]).reset_index().rename(columns={'index':'UTC time'})
        else:
            df_building_processed = df_kwe_full.reset_index().rename(columns={'index':'UTC time'})
            df_building_errors_missing = df_kwe_errors_missing.reset_index().rename(columns={'index':'UTC time'})
    else:
         if not df_ilima_full.empty:
            df_building_processed = df_ilima_full.reset_index().rename(columns={'index':'UTC time'})
            df_building_errors_missing = df_ilima_errors_missing.reset_index().rename(columns={'index':'UTC time'})
column_list = ['UTC time','Ceiling_Fans_kW','Ceiling_Fans_kWh','floor_surface_temperature_center','plenum_temperature_center','Condensing_Unit_kW','Condensing_Unit_kWh','air_temperature_e','wall_surface_temperature_e','Exhaust_Fan_kW','Exhaust_Fan_kWh','Fan_Coil_Unit_kW','Fan_Coil_Unit_kWh','Lighting_Main_Space_kW','Lighting_Main_Space_kWh','floor_surface_temperature_nw','plenum_temperature_nw','PV_production_kW','PV_production_kWh','unmonitored_end_uses_kW','unmonitored_end_uses_kWh','total_consumption_kW','total_consumption_kWh','net_consumption_kW','net_consumption_kWh','Room_Air_CO2','Room_Air_Humidity','Room_Air_Speed','Room_Illuminance_Ceiling','Room_Illuminance_WestWall','floor_surface_temperature_se','plenum_temperature_se','Supply_Air_Temperature_C','Supply_Air_Humidity','air_temperature_w','wall_surface_temperature_w','Source','Lighting_Wall_and_Exterior_kW','Lighting_Wall_and_Exterior_kWh','Louver_Actuator_kW','Louver_Actuator_kWh','Room_Air_Temperature_C']
if not df_building_processed.empty:    
    df_building_processed = df_building_processed.reindex(columns=column_list)
    df_building_processed['Source'] = df_building_processed['Source'].astype(int)
if not df_building_errors_missing.empty:    
    df_building_errors_missing = df_building_errors_missing.reindex(columns=column_list)
    df_building_errors_missing['Source'] = df_building_errors_missing['Source'].astype(int)
if not df_kw_full.empty:
    if not df_ewa_full.empty:
        df_weather_processed = pd.concat([df_kw_full,df_ewa_full]).reset_index().rename(columns={'index':'UTC time'})
        df_weather_errors_missing = pd.concat([df_kw_errors_missing,df_ewa_errors_missing]).reset_index().rename(columns={'index':'UTC time'})
    else:
        df_weather_processed = df_kw_full.reset_index().rename(columns={'index':'UTC time'})
        df_weather_errors_missing = df_kw_errors_missing.reset_index().rename(columns={'index':'UTC time'})
else:
    if not df_ewa_full.empty:
        df_weather_processed = df_ewa_full.reset_index().rename(columns={'index':'UTC time'})
        df_weather_errors_missing = df_ewa_errors_missing.reset_index().rename(columns={'index':'UTC time'})
if not df_weather_processed.empty:    
    df_weather_processed = df_weather_processed[['UTC time','Batt','DewPt','Gust Speed','Pressure','RH','Rain','Solar Rad','Solar Rad1','Source','Temp','Wind Dir','Wind Speed']]
    df_weather_processed['Source'] = df_weather_processed['Source'].astype(int)
if not df_weather_errors_missing.empty:    
    df_weather_errors_missing = df_weather_errors_missing[['UTC time','Batt','DewPt','Gust Speed','Pressure','RH','Rain','Solar Rad','Solar Rad1','Source','Temp','Wind Dir','Wind Speed']]
    df_weather_errors_missing['Source'] = df_weather_errors_missing['Source'].astype(int)
hours = []
minutes = []
for t in pd.DatetimeIndex(df_building_processed['UTC time']):
    hours.append(datetime(t.year,t.month,t.day,t.hour,0,0,tzinfo=timezone.utc).strftime("%Y-%m-%d %H:%M:%S"))
    minutes.append(datetime(t.year,t.month,t.day,t.hour,t.minute,0,tzinfo=timezone.utc).strftime("%Y-%m-%d %H:%M:%S"))
df_building_processed_group = df_building_processed.assign(hour=hours, minute=minutes)
hours = []
minutes = []
for t in pd.DatetimeIndex(df_weather_processed['UTC time']):
    hours.append(datetime(t.year,t.month,t.day,t.hour,0,0,tzinfo=timezone.utc).strftime("%Y-%m-%d %H:%M:%S"))
    minutes.append(datetime(t.year,t.month,t.day,t.hour,t.minute,0,tzinfo=timezone.utc).strftime("%Y-%m-%d %H:%M:%S"))
df_weather_processed_group = df_weather_processed.assign(hour=hours, minute=minutes)
df_weather_processed_group.rename(columns={'Gust Speed':'gust_speed', 'Solar Rad':'solar_rad', 'Solar Rad1':'solar_rad1', 'Wind Dir':'wind_dir', 'Wind Speed':'wind_speed'}, inplace=True)
building_columns = [x for x in list(df_building_processed_group.columns) if x not in ['UTC time', 'hour', 'Source', 'minute']]
df_building_processed_hour = pd.melt(df_building_processed_group.groupby(['hour', 'Source'])[building_columns].max().reset_index(), id_vars=['hour', 'Source']).rename(columns={'hour':'date_time', 'Source':'source', 'variable':'sensor', 'value':'max'})
df_building_processed_hour['min'] = pd.melt(df_building_processed_group.groupby(['hour', 'Source'])[building_columns].min().reset_index(), id_vars=['hour', 'Source']).drop(['hour', 'Source', 'variable'], axis=1)
df_building_processed_hour['y'] = pd.melt(df_building_processed_group.groupby(['hour', 'Source'])[building_columns].mean().reset_index(), id_vars=['hour', 'Source']).drop(['hour', 'Source', 'variable'], axis=1)
df_building_processed_hour = df_building_processed_hour[['y', 'max', 'min', 'sensor', 'source', 'date_time']]
df_building_processed_hour['sensor'] = df_building_processed_hour['sensor'].str.lower()
df_building_processed_minute = pd.melt(df_building_processed_group.groupby(['minute', 'Source'])[building_columns].max().reset_index(), id_vars=['minute', 'Source']).rename(columns={'minute':'date_time', 'Source':'source', 'variable':'sensor', 'value':'max'})
df_building_processed_minute['min'] = pd.melt(df_building_processed_group.groupby(['minute', 'Source'])[building_columns].min().reset_index(), id_vars=['minute', 'Source']).drop(['minute', 'Source', 'variable'], axis=1)
df_building_processed_minute['y'] = pd.melt(df_building_processed_group.groupby(['minute', 'Source'])[building_columns].mean().reset_index(), id_vars=['minute', 'Source']).drop(['minute', 'Source', 'variable'], axis=1)
df_building_processed_minute = df_building_processed_minute[['y', 'max', 'min', 'sensor', 'source', 'date_time']]
df_building_processed_minute['sensor'] = df_building_processed_minute['sensor'].str.lower()
weather_columns = [x for x in list(df_weather_processed_group.columns) if x not in ['UTC time', 'hour', 'Source', 'minute']]
df_weather_processed_hour = pd.melt(df_weather_processed_group.groupby(['hour', 'Source'])[weather_columns].max().reset_index(), id_vars=['hour', 'Source']).rename(columns={'hour':'date_time', 'Source':'source', 'variable':'sensor', 'value':'max'})
df_weather_processed_hour['min'] = pd.melt(df_weather_processed_group.groupby(['hour', 'Source'])[weather_columns].min().reset_index(), id_vars=['hour', 'Source']).drop(['hour', 'Source', 'variable'], axis=1)
df_weather_processed_hour['y'] = pd.melt(df_weather_processed_group.groupby(['hour', 'Source'])[weather_columns].mean().reset_index(), id_vars=['hour', 'Source']).drop(['hour', 'Source', 'variable'], axis=1)
df_weather_processed_hour = df_weather_processed_hour[['y', 'max', 'min', 'sensor', 'source', 'date_time']]
df_weather_processed_hour['sensor'] = df_weather_processed_hour['sensor'].str.lower()
df_weather_processed_minute = pd.melt(df_weather_processed_group.groupby(['minute', 'Source'])[weather_columns].max().reset_index(), id_vars=['minute', 'Source']).rename(columns={'minute':'date_time', 'Source':'source', 'variable':'sensor', 'value':'max'})
df_weather_processed_minute['min'] = pd.melt(df_weather_processed_group.groupby(['minute', 'Source'])[weather_columns].min().reset_index(), id_vars=['minute', 'Source']).drop(['minute', 'Source', 'variable'], axis=1)
df_weather_processed_minute['y'] = pd.melt(df_weather_processed_group.groupby(['minute', 'Source'])[weather_columns].mean().reset_index(), id_vars=['minute', 'Source']).drop(['minute', 'Source', 'variable'], axis=1)
df_weather_processed_minute = df_weather_processed_minute[['y', 'max', 'min', 'sensor', 'source', 'date_time']]
df_weather_processed_minute['sensor'] = df_weather_processed_minute['sensor'].str.lower()
if not df_building_processed.empty:    
    upload_file_path_building = 'ilima_kawaikini_building_data_processed_monthly/ilima_kawaikini_building_data_processed_' + str(now.year) + '_' + str(now.month) + '.csv.gz'
    df_building_processed.to_csv(upload_file_path_building, compression='gzip', header=False, index=False)
    mp = bucket.initiate_multipart_upload(upload_file_path_building)
    source_size = os.stat(upload_file_path_building).st_size
    chunk_count = int(math.ceil(source_size / float(chunk_size)))
    for i in range(chunk_count):
        offset = chunk_size * i
        bytes = min(chunk_size, source_size - offset)
        with FileChunkIO(upload_file_path_building, 'r', offset=offset, bytes=bytes) as fp:
            mp.upload_part_from_file(fp, part_num=i + 1)
    mp.complete_upload()
    os.remove(upload_file_path_building)
if not df_building_processed_hour.empty:    
    upload_file_path_building_hour = 'ilima_kawaikini_building_data_processed_monthly_hour/ilima_kawaikini_building_data_processed_hour_' + str(now.year) + '_' + str(now.month) + '.csv.gz'
    df_building_processed_hour.to_csv(upload_file_path_building_hour, compression='gzip', header=False, index=False)
    mp = bucket.initiate_multipart_upload(upload_file_path_building_hour)
    source_size = os.stat(upload_file_path_building_hour).st_size
    chunk_count = int(math.ceil(source_size / float(chunk_size)))
    for i in range(chunk_count):
        offset = chunk_size * i
        bytes = min(chunk_size, source_size - offset)
        with FileChunkIO(upload_file_path_building_hour, 'r', offset=offset, bytes=bytes) as fp:
            mp.upload_part_from_file(fp, part_num=i + 1)
    mp.complete_upload()
    os.remove(upload_file_path_building_hour)    
if not df_building_processed_minute.empty:    
    upload_file_path_building_minute = 'ilima_kawaikini_building_data_processed_monthly_minute/ilima_kawaikini_building_data_processed_minute_' + str(now.year) + '_' + str(now.month) + '.csv.gz'
    df_building_processed_minute.to_csv(upload_file_path_building_minute, compression='gzip', header=False, index=False)
    mp = bucket.initiate_multipart_upload(upload_file_path_building_minute)
    source_size = os.stat(upload_file_path_building_minute).st_size
    chunk_count = int(math.ceil(source_size / float(chunk_size)))
    for i in range(chunk_count):
        offset = chunk_size * i
        bytes = min(chunk_size, source_size - offset)
        with FileChunkIO(upload_file_path_building_minute, 'r', offset=offset, bytes=bytes) as fp:
            mp.upload_part_from_file(fp, part_num=i + 1)
    mp.complete_upload()
    os.remove(upload_file_path_building_minute)        
if not df_building_errors_missing.empty:    
    upload_file_path_building_errors_missing = 'ilima_kawaikini_building_errors_missing_monthly/ilima_kawaikini_building_errors_missing_' + str(now.year) + '_' + str(now.month) + '.csv.gz'
    df_building_errors_missing.to_csv(upload_file_path_building_errors_missing, compression='gzip', header=False, index=False)
    mp = bucket.initiate_multipart_upload(upload_file_path_building_errors_missing)
    source_size = os.stat(upload_file_path_building_errors_missing).st_size
    chunk_count = int(math.ceil(source_size / float(chunk_size)))
    for i in range(chunk_count):
        offset = chunk_size * i
        bytes = min(chunk_size, source_size - offset)
        with FileChunkIO(upload_file_path_building_errors_missing, 'r', offset=offset, bytes=bytes) as fp:
            mp.upload_part_from_file(fp, part_num=i + 1)
    mp.complete_upload()
    os.remove(upload_file_path_building_errors_missing)
if not df_weather_processed.empty:
    upload_file_path_weather = 'ilima_kawaikini_weather_data_processed_monthly/ilima_kawaikini_weather_data_processed_' + str(now.year) + '_' + str(now.month) + '.csv.gz'
    df_weather_processed.to_csv(upload_file_path_weather, compression='gzip', header=False, index=False)
    mp = bucket.initiate_multipart_upload(upload_file_path_weather)
    source_size = os.stat(upload_file_path_weather).st_size
    chunk_count = int(math.ceil(source_size / float(chunk_size)))
    for i in range(chunk_count):
        offset = chunk_size * i
        bytes = min(chunk_size, source_size - offset)
        with FileChunkIO(upload_file_path_weather, 'r', offset=offset, bytes=bytes) as fp:
            mp.upload_part_from_file(fp, part_num=i + 1)
    mp.complete_upload()
    os.remove(upload_file_path_weather)    
if not df_weather_processed_hour.empty:    
    upload_file_path_weather_hour = 'ilima_kawaikini_weather_data_processed_monthly_hour/ilima_kawaikini_weather_data_processed_hour_' + str(now.year) + '_' + str(now.month) + '.csv.gz'
    df_weather_processed_hour.to_csv(upload_file_path_weather_hour, compression='gzip', header=False, index=False)
    mp = bucket.initiate_multipart_upload(upload_file_path_weather_hour)
    source_size = os.stat(upload_file_path_weather_hour).st_size
    chunk_count = int(math.ceil(source_size / float(chunk_size)))
    for i in range(chunk_count):
        offset = chunk_size * i
        bytes = min(chunk_size, source_size - offset)
        with FileChunkIO(upload_file_path_weather_hour, 'r', offset=offset, bytes=bytes) as fp:
            mp.upload_part_from_file(fp, part_num=i + 1)
    mp.complete_upload()
    os.remove(upload_file_path_weather_hour)    
if not df_weather_processed_minute.empty:    
    upload_file_path_weather_minute = 'ilima_kawaikini_weather_data_processed_monthly_minute/ilima_kawaikini_weather_data_processed_minute_' + str(now.year) + '_' + str(now.month) + '.csv.gz'
    df_weather_processed_minute.to_csv(upload_file_path_weather_minute, compression='gzip', header=False, index=False)
    mp = bucket.initiate_multipart_upload(upload_file_path_weather_minute)
    source_size = os.stat(upload_file_path_weather_minute).st_size
    chunk_count = int(math.ceil(source_size / float(chunk_size)))
    for i in range(chunk_count):
        offset = chunk_size * i
        bytes = min(chunk_size, source_size - offset)
        with FileChunkIO(upload_file_path_weather_minute, 'r', offset=offset, bytes=bytes) as fp:
            mp.upload_part_from_file(fp, part_num=i + 1)
    mp.complete_upload()
    os.remove(upload_file_path_weather_minute)        
if not df_weather_errors_missing.empty:    
    upload_file_path_weather_errors_missing = 'ilima_kawaikini_weather_errors_missing_monthly/ilima_kawaikini_weather_errors_missing_' + str(now.year) + '_' + str(now.month) + '.csv.gz'
    df_weather_errors_missing.to_csv(upload_file_path_weather_errors_missing, compression='gzip', header=False, index=False)
    mp = bucket.initiate_multipart_upload(upload_file_path_weather_errors_missing)
    source_size = os.stat(upload_file_path_weather_errors_missing).st_size
    chunk_count = int(math.ceil(source_size / float(chunk_size)))
    for i in range(chunk_count):
        offset = chunk_size * i
        bytes = min(chunk_size, source_size - offset)
        with FileChunkIO(upload_file_path_weather_errors_missing, 'r', offset=offset, bytes=bytes) as fp:
            mp.upload_part_from_file(fp, part_num=i + 1)
    mp.complete_upload()
    os.remove(upload_file_path_weather_errors_missing)
print(datetime.now(timezone.utc).strftime("%Y-%m-%d %H:%M:%S") + ' (UTC): validating data before loading from S3 to Redshift')
psycopg2_redshift_conn = psycopg2.connect(dbname= 'devimport', host='rh1-data.cgptfmcyizib.us-west-2.redshift.amazonaws.com', port='5439',user='admin', password='4AdaPT118899-DAPt4')
cur = psycopg2_redshift_conn.cursor()
if cur.execute("""copy ilima_kawaikini_building_data_reported from 's3://rh1-extract/ilima_kawaikini_building_data_reported_monthly/' iam_role 'arn:aws:iam::051743098208:role/RedshiftCopyUnload' csv gzip NOLOAD;""") == None:
    print(datetime.now(timezone.utc).strftime("%Y-%m-%d %H:%M:%S") + ' (UTC): <DATA VALID> building_data_reported')
    cur.execute("""delete ilima_kawaikini_building_data_reported""")
    cur.execute("""copy ilima_kawaikini_building_data_reported from 's3://rh1-extract/ilima_kawaikini_building_data_reported_monthly/' iam_role 'arn:aws:iam::051743098208:role/RedshiftCopyUnload' csv gzip;""")
    cur.execute("""select query, filename, curtime from stl_load_commits where filename like '%ilima_kawaikini_building_data_reported%' order by query desc""")
    df = pd.DataFrame(cur.fetchall(), columns=['query', 'filename', 'curtime']).groupby('query').max()[-1:]
    print(df.reset_index()['curtime'][0].strftime("%Y-%m-%d %H:%M:%S") + ' (UTC): <LOAD VERIFIED> ' + df.reset_index()['filename'][0][33:])
if cur.execute("""copy ilima_kawaikini_building_data_processed from 's3://rh1-extract/ilima_kawaikini_building_data_processed_monthly/' iam_role 'arn:aws:iam::051743098208:role/RedshiftCopyUnload' csv gzip NOLOAD;""") == None:
    print(datetime.now(timezone.utc).strftime("%Y-%m-%d %H:%M:%S") + ' (UTC): <DATA VALID> building_data_processed')
    cur.execute("""delete ilima_kawaikini_building_data_processed""")
    cur.execute("""copy ilima_kawaikini_building_data_processed from 's3://rh1-extract/ilima_kawaikini_building_data_processed_monthly/' iam_role 'arn:aws:iam::051743098208:role/RedshiftCopyUnload' csv gzip;""")
    cur.execute("""select query, filename, curtime from stl_load_commits where filename like '%ilima_kawaikini_building_data_processed%' order by query desc""")
    df = pd.DataFrame(cur.fetchall(), columns=['query', 'filename', 'curtime']).groupby('query').max()[-1:]
    print(df.reset_index()['curtime'][0].strftime("%Y-%m-%d %H:%M:%S") + ' (UTC): <LOAD VERIFIED> ' + df.reset_index()['filename'][0][33:])    
if cur.execute("""copy ilima_kawaikini_building_data_processed_hour from 's3://rh1-extract/ilima_kawaikini_building_data_processed_monthly_hour/' iam_role 'arn:aws:iam::051743098208:role/RedshiftCopyUnload' csv gzip NOLOAD;""") == None:
    print(datetime.now(timezone.utc).strftime("%Y-%m-%d %H:%M:%S") + ' (UTC): <DATA VALID> building_data_processed_hour')    
    cur.execute("""delete ilima_kawaikini_building_data_processed_hour""")
    cur.execute("""copy ilima_kawaikini_building_data_processed_hour from 's3://rh1-extract/ilima_kawaikini_building_data_processed_monthly_hour/' iam_role 'arn:aws:iam::051743098208:role/RedshiftCopyUnload' csv gzip;""")
    cur.execute("""select query, filename, curtime from stl_load_commits where filename like '%ilima_kawaikini_building_data_processed_hour%' order by query desc""")
    df = pd.DataFrame(cur.fetchall(), columns=['query', 'filename', 'curtime']).groupby('query').max()[-1:]
    print(df.reset_index()['curtime'][0].strftime("%Y-%m-%d %H:%M:%S") + ' (UTC): <LOAD VERIFIED> ' + df.reset_index()['filename'][0][33:])
if cur.execute("""copy ilima_kawaikini_building_data_processed_minute from 's3://rh1-extract/ilima_kawaikini_building_data_processed_monthly_minute/' iam_role 'arn:aws:iam::051743098208:role/RedshiftCopyUnload' csv gzip NOLOAD;""") == None:
    print(datetime.now(timezone.utc).strftime("%Y-%m-%d %H:%M:%S") + ' (UTC): <DATA VALID> building_data_processed_minute')
    cur.execute("""delete ilima_kawaikini_building_data_processed_minute""")
    cur.execute("""copy ilima_kawaikini_building_data_processed_minute from 's3://rh1-extract/ilima_kawaikini_building_data_processed_monthly_minute/' iam_role 'arn:aws:iam::051743098208:role/RedshiftCopyUnload' csv gzip;""")
    cur.execute("""select query, filename, curtime from stl_load_commits where filename like '%ilima_kawaikini_building_data_processed_minute%' order by query desc""")
    df = pd.DataFrame(cur.fetchall(), columns=['query', 'filename', 'curtime']).groupby('query').max()[-1:]
    print(df.reset_index()['curtime'][0].strftime("%Y-%m-%d %H:%M:%S") + ' (UTC): <LOAD VERIFIED> ' + df.reset_index()['filename'][0][33:])
if cur.execute("""copy ilima_kawaikini_building_errors_missing from 's3://rh1-extract/ilima_kawaikini_building_errors_missing_monthly/' iam_role 'arn:aws:iam::051743098208:role/RedshiftCopyUnload' csv gzip NOLOAD;""") == None:
    print(datetime.now(timezone.utc).strftime("%Y-%m-%d %H:%M:%S") + ' (UTC): <DATA VALID> building_errors_missing')
    cur.execute("""delete ilima_kawaikini_building_errors_missing""")
    cur.execute("""copy ilima_kawaikini_building_errors_missing from 's3://rh1-extract/ilima_kawaikini_building_errors_missing_monthly/' iam_role 'arn:aws:iam::051743098208:role/RedshiftCopyUnload' csv gzip;""")
    cur.execute("""select query, filename, curtime from stl_load_commits where filename like '%ilima_kawaikini_building_errors_missing%' order by query desc""")
    df = pd.DataFrame(cur.fetchall(), columns=['query', 'filename', 'curtime']).groupby('query').max()[-1:]
    print(df.reset_index()['curtime'][0].strftime("%Y-%m-%d %H:%M:%S") + ' (UTC): <LOAD VERIFIED> ' + df.reset_index()['filename'][0][33:])
if cur.execute("""copy ilima_kawaikini_weather_data_reported from 's3://rh1-extract/ilima_kawaikini_weather_data_reported_monthly/' iam_role 'arn:aws:iam::051743098208:role/RedshiftCopyUnload' csv gzip NOLOAD;""") == None:
    print(datetime.now(timezone.utc).strftime("%Y-%m-%d %H:%M:%S") + ' (UTC): <DATA VALID> weather_data_reported')
    cur.execute("""delete ilima_kawaikini_weather_data_reported""")
    cur.execute("""copy ilima_kawaikini_weather_data_reported from 's3://rh1-extract/ilima_kawaikini_weather_data_reported_monthly/' iam_role 'arn:aws:iam::051743098208:role/RedshiftCopyUnload' csv gzip;""")
    cur.execute("""select query, filename, curtime from stl_load_commits where filename like '%ilima_kawaikini_weather_data_reported%' order by query desc""")
    df = pd.DataFrame(cur.fetchall(), columns=['query', 'filename', 'curtime']).groupby('query').max()[-1:]
    print(df.reset_index()['curtime'][0].strftime("%Y-%m-%d %H:%M:%S") + ' (UTC): <LOAD VERIFIED> ' + df.reset_index()['filename'][0][33:])          
if cur.execute("""copy ilima_kawaikini_weather_data_processed from 's3://rh1-extract/ilima_kawaikini_weather_data_processed_monthly/' iam_role 'arn:aws:iam::051743098208:role/RedshiftCopyUnload' csv gzip NOLOAD;""") == None:
    print(datetime.now(timezone.utc).strftime("%Y-%m-%d %H:%M:%S") + ' (UTC): <DATA VALID> weather_data_processed')
    cur.execute("""delete ilima_kawaikini_weather_data_processed""")
    cur.execute("""copy ilima_kawaikini_weather_data_processed from 's3://rh1-extract/ilima_kawaikini_weather_data_processed_monthly/' iam_role 'arn:aws:iam::051743098208:role/RedshiftCopyUnload' csv gzip;""")
    cur.execute("""select query, filename, curtime from stl_load_commits where filename like '%ilima_kawaikini_weather_data_processed%' order by query desc""")
    df = pd.DataFrame(cur.fetchall(), columns=['query', 'filename', 'curtime']).groupby('query').max()[-1:]
    print(df.reset_index()['curtime'][0].strftime("%Y-%m-%d %H:%M:%S") + ' (UTC): <LOAD VERIFIED> ' + df.reset_index()['filename'][0][33:])
if cur.execute("""copy ilima_kawaikini_weather_data_processed_hour from 's3://rh1-extract/ilima_kawaikini_weather_data_processed_monthly_hour/' iam_role 'arn:aws:iam::051743098208:role/RedshiftCopyUnload' csv gzip NOLOAD;""") == None:
    print(datetime.now(timezone.utc).strftime("%Y-%m-%d %H:%M:%S") + ' (UTC): <DATA VALID> weather_data_processed_hour')
    cur.execute("""delete ilima_kawaikini_weather_data_processed_hour""")
    cur.execute("""copy ilima_kawaikini_weather_data_processed_hour from 's3://rh1-extract/ilima_kawaikini_weather_data_processed_monthly_hour/' iam_role 'arn:aws:iam::051743098208:role/RedshiftCopyUnload' csv gzip;""")
    cur.execute("""select query, filename, curtime from stl_load_commits where filename like '%ilima_kawaikini_weather_data_processed_hour%' order by query desc""")
    df = pd.DataFrame(cur.fetchall(), columns=['query', 'filename', 'curtime']).groupby('query').max()[-1:]
    print(df.reset_index()['curtime'][0].strftime("%Y-%m-%d %H:%M:%S") + ' (UTC): <LOAD VERIFIED> ' + df.reset_index()['filename'][0][33:])                
if cur.execute("""copy ilima_kawaikini_weather_data_processed_minute from 's3://rh1-extract/ilima_kawaikini_weather_data_processed_monthly_minute/' iam_role 'arn:aws:iam::051743098208:role/RedshiftCopyUnload' csv gzip NOLOAD;""") == None:
    print(datetime.now(timezone.utc).strftime("%Y-%m-%d %H:%M:%S") + ' (UTC): <DATA VALID> weather_data_processed_minute')
    cur.execute("""delete ilima_kawaikini_weather_data_processed_minute""")
    cur.execute("""copy ilima_kawaikini_weather_data_processed_minute from 's3://rh1-extract/ilima_kawaikini_weather_data_processed_monthly_minute/' iam_role 'arn:aws:iam::051743098208:role/RedshiftCopyUnload' csv gzip;""")
    cur.execute("""select query, filename, curtime from stl_load_commits where filename like '%ilima_kawaikini_weather_data_processed_minute%' order by query desc""")
    df = pd.DataFrame(cur.fetchall(), columns=['query', 'filename', 'curtime']).groupby('query').max()[-1:]
    print(df.reset_index()['curtime'][0].strftime("%Y-%m-%d %H:%M:%S") + ' (UTC): <LOAD VERIFIED> ' + df.reset_index()['filename'][0][33:])        
if cur.execute("""copy ilima_kawaikini_weather_errors_missing from 's3://rh1-extract/ilima_kawaikini_weather_errors_missing_monthly/' iam_role 'arn:aws:iam::051743098208:role/RedshiftCopyUnload' csv gzip NOLOAD;""") == None:
    print(datetime.now(timezone.utc).strftime("%Y-%m-%d %H:%M:%S") + ' (UTC): <DATA VALID> weather_errors_missing')
    cur.execute("""delete ilima_kawaikini_weather_errors_missing""")
    cur.execute("""copy ilima_kawaikini_weather_errors_missing from 's3://rh1-extract/ilima_kawaikini_weather_errors_missing_monthly/' iam_role 'arn:aws:iam::051743098208:role/RedshiftCopyUnload' csv gzip;""")
    cur.execute("""select query, filename, curtime from stl_load_commits where filename like '%ilima_kawaikini_weather_errors_missing%' order by query desc""")
    df = pd.DataFrame(cur.fetchall(), columns=['query', 'filename', 'curtime']).groupby('query').max()[-1:]
    print(df.reset_index()['curtime'][0].strftime("%Y-%m-%d %H:%M:%S") + ' (UTC): <LOAD VERIFIED> ' + df.reset_index()['filename'][0][33:])
psycopg2_redshift_conn.commit()
psycopg2_redshift_conn.close()
print(datetime.now(timezone.utc).strftime("%Y-%m-%d %H:%M:%S") + ' (UTC): finished')
print(now.strftime('%Y') + " " + now.strftime('%B') + ' KW WEST Expected: ' + str(df_kww_full.shape[0]*38) + ' Returned: ' + str(kww_good+kww_missing+kww_high+kww_low) + ' Missing: ' + str(kww_missing) + ' High: ' + str(kww_high) + ' Low: ' + str(kww_low))
print(now.strftime('%Y') + " " + now.strftime('%B') + ' KW EAST Expected: ' + str(df_kwe_full.shape[0]*40) + ' Returned: ' + str(kwe_good+kwe_missing+kwe_high+kwe_low) + ' Missing: ' + str(kwe_missing) + ' High: ' + str(kwe_high) + ' Low: ' + str(kwe_low))
print(now.strftime('%Y') + " " + now.strftime('%B') + ' ILIMA Expected: ' + str(df_ilima_full.shape[0]*37) + ' Returned: ' + str(ilima_good+ilima_missing+ilima_high+ilima_low) + ' Missing: ' + str(ilima_missing) + ' High: ' + str(ilima_high) + ' Low: ' + str(ilima_low))
print(now.strftime('%Y') + " " + now.strftime('%B') + ' KW WEATHER Expected: ' + str(df_kw_full.shape[0]*11) + ' Returned: ' + str(kw_good+kw_missing+kw_high+kw_low) + ' Missing: ' + str(kw_missing) + ' High: ' + str(kw_high) + ' Low: ' + str(kw_low))
print(now.strftime('%Y') + " " + now.strftime('%B') + ' EWA WEATHER Expected: ' + str(df_ewa_full.shape[0]*11) + ' Returned: ' + str(ewa_good+ewa_missing+ewa_high+ewa_low) + ' Missing: ' + str(ewa_missing) + ' High: ' + str(ewa_high) + ' Low: ' + str(ewa_low))